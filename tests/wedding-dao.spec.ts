import { client } from "../src/connection";
import { WeddingDAO } from "../src/daos/wedding-dao";
import { WeddingDaoPostgres } from "../src/daos/wedding-dao-postgres";
import { Wedding } from "../src/entities";


const weddingDAO:WeddingDAO = new WeddingDaoPostgres();

const testWedding:Wedding = new Wedding(0, "09/02/21", "somewhere", "create testwedding", 25000);

test("Create a Wedding", async()=>{
    const result:Wedding = await weddingDAO.createWedding(testWedding);
    expect(result.wId).not.toBe(0);
});

test("Get wedding by Id", async ()=>{
    let wedding:Wedding = new Wedding(0, "09/03/21", "somewhere", "get testwedding", 25000);
    wedding = await weddingDAO.createWedding(wedding);

    let retrievedWedding:Wedding = await weddingDAO.getWeddingById(wedding.wId)

    expect(retrievedWedding.wName).toBe(wedding.wName);
});

test("Get all weddings", async ()=>{
    let wedding1:Wedding = new Wedding(0, "09/01/21", "somewhere", "get all testwedding1", 25000);
    let wedding2:Wedding = new Wedding(0, "09/02/21", "somewhere", "get all testwedding2", 30000);
    let wedding3:Wedding = new Wedding(0, "09/03/21", "somewhere", "get all testwedding3", 35000);
    await weddingDAO.createWedding(wedding1);
    await weddingDAO.createWedding(wedding2);
    await weddingDAO.createWedding(wedding3);

    const weddings:Wedding[] = await weddingDAO.getAllWeddings();

    expect(weddings.length).toBeGreaterThanOrEqual(3);
});

test("Update wedding", async ()=>{
    let wedding:Wedding = new Wedding(0, "09/02/21", "somewhere", "random name", 25000);
    wedding = await weddingDAO.createWedding(wedding);
    const weddingId = wedding.wId;
    // to update an object we just edit it and then pass it into a method
    wedding.wName = "certain name";
    wedding = await weddingDAO.updateWedding(wedding, weddingId);

    const updatedWedding = await weddingDAO.getWeddingById(wedding.wId);
    expect(updatedWedding.wName).toBe("certain name");
});

test("Delete wedding by id", async ()=>{
    let wedding:Wedding = new Wedding(0, "09/02/21", "somewhere", "somename", 25000);
    wedding = await weddingDAO.createWedding(wedding);

    const result:boolean = await weddingDAO.deleteWeddingById(wedding.wId);
    expect(result).toBeTruthy()

});


afterAll(async()=>{
    client.end();
});
import { client } from "../src/connection";
import { ExpenseDAO } from "../src/daos/expense-dao";
import { ExpenseDaoPostgres } from "../src/daos/expense-dao-postgres";
import { WeddingDaoPostgres } from "../src/daos/wedding-dao-postgres";
import { Expense, Wedding } from "../src/entities";
import { WeddingDAO } from "../src/daos/wedding-dao";

const expenseDAO:ExpenseDAO = new ExpenseDaoPostgres();
const weddingDAO:WeddingDAO = new WeddingDaoPostgres();

const testExpense:Expense = new Expense(0, "Test Reason", 1000000, 3);

test("Create an expense", async ()=>{
    let wedding1:Wedding = new Wedding(0, "09/16/21", "anywhere", "JestWedding", 250000)
    wedding1 = await weddingDAO.createWedding(wedding1);
    let wedding2:Wedding = new Wedding(0, "09/15/21", "anywhere2", "JestWedding2", 260000)
    wedding2 = await weddingDAO.createWedding(wedding2);

    const result:Expense = await expenseDAO.createExpense(testExpense);
    console.log(result);
    expect(result.eId).not.toBe(0);
})

test("Get expense by Id", async ()=>{
    let expense:Expense = new Expense(0, "Test Reason2", 2000000, 3);
    expense = await expenseDAO.createExpense(expense);

    let retrievedExpense:Expense = await expenseDAO.getExpenseById(expense.eId);

    expect(retrievedExpense.amount).toBe(expense.amount);
});

test("Get all expenses", async ()=>{
    let expense1:Expense = new Expense(0, "Test Reason3", 3000000, 3);
    let expense2:Expense = new Expense(0, "Test Reason4", 4000000, 3);
    let expense3:Expense = new Expense(0, "Test Reason5", 5000000, 3);
    await expenseDAO.createExpense(expense1);
    await expenseDAO.createExpense(expense2);
    await expenseDAO.createExpense(expense3);

    const expenses:Expense[] = await expenseDAO.getAllExpenses();

    expect(expenses.length).toBeGreaterThanOrEqual(3);
})

test("Get expenses by wId", async ()=>{
    let expense1:Expense = new Expense(0, "Test Reason6", 6000000, 3);
    let expense2:Expense = new Expense(0, "Test Reason7", 7000000, 3);
    let expense3:Expense = new Expense(0, "Test Reason8", 8000000, 3);
    await expenseDAO.createExpense(expense1);
    await expenseDAO.createExpense(expense2);
    await expenseDAO.createExpense(expense3);

    const expenses:Expense[] = await expenseDAO.getExpensesByWid(3);

    expect (expenses.length).toBeGreaterThanOrEqual(3);
});

test("Update expense", async ()=>{
    let expense:Expense = new Expense(0, "Test Reason9", 9000000, 3);
    expense = await expenseDAO.createExpense(expense);

    // to update an object we just edit it and then pass it into a method
    expense.reason = "New Reason9";
    expense.amount = 9100000;
    expense = await expenseDAO.updateExpense(expense);

    const updatedExpense = await expenseDAO.getExpenseById(expense.eId);
    expect(updatedExpense.amount).toBe(9100000);
});

test("Delete expense by id", async ()=>{
    let expense:Expense = new Expense(0, "Test Reason10", 10000000, 3);
    expense = await expenseDAO.createExpense(expense);

    const result:boolean = await expenseDAO.deleteExpenseById(expense.eId);
    expect(result).toBeTruthy()

});



afterAll(async()=>{
    client.end();
})
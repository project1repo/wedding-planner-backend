
export class Wedding{
    constructor(
        public wId:number,
        public wDate:string,
        public wLocation:string,
        public wName:string,
        public wBudget:number
    ){}
}

export class Expense{
    constructor(
        public eId:number,
        public reason:string,
        public amount:number,
        public weddingId:number
    ){}
}
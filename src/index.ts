import express from 'express';
import { Expense, Wedding } from './entities';
import { MissingResourceError } from './errors';
import ExpenseService from './services/expense-service';
import { ExpenseServiceImpl } from './services/expense-service-impl';
import WeddingService from './services/wedding-service';
import { WeddingServiceImpl } from './services/wedding-service-impl';
import cors from 'cors';


const app = express()
app.use(express.json());
app.use(cors())

const weddingService:WeddingService = new WeddingServiceImpl();
const expenseService:ExpenseService = new ExpenseServiceImpl();

// WEDDINGS

// Get weddings
app.get("/weddings", async (req, res)=>{
    const weddings:Wedding[] = await weddingService.retrieveAllWeddings();
    res.send(weddings);
    res.status(200)
});

// Get wedding by Id
app.get("/weddings/:id", async (req, res) =>{
    try{
        const weddingId = Number(req.params.id);
        const wedding:Wedding = await weddingService.retrieveWeddingById(weddingId);
        res.send(wedding);
    }catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// Post wedding
app.post("/weddings", async (req, res) =>{
    let wedding:Wedding = req.body;
    wedding = await weddingService.registerWedding(wedding);
    res.status(201);
    res.send(wedding);
});

// Put wedding
app.put("/weddings/:id", async (req, res) =>{
    try{
        const newWedding:Wedding = req.body;
        const weddingId = Number(req.params.id);
        const wedding:Wedding = await weddingService.modifyWedding(newWedding, weddingId);
        res.send(wedding);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

// Delete wedding
app.delete("/weddings/:id", async (req, res) =>{
    try{
        const weddingId = Number(req.params.id);
        const success:Boolean = await weddingService.removeWeddingById(weddingId);
        res.status(205);
        res.send(success);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});


// EXPENSES

// Get expenses
app.get("/expenses", async (req, res)=>{
    const expenses:Expense[] = await expenseService.retrieveAllExpenses();
    res.send(expenses);
    res.status(200)
});

// Get expense by Id
app.get("/expenses/:id", async (req, res) =>{
    try{
        const expenseId = Number(req.params.id);
        const expense:Expense = await expenseService.retrieveExpenseById(expenseId);
        res.send(expense);
    }catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// Get expenses by wId
app.get("/weddings/:wid/expenses", async (req, res) =>{
    try{
        const expenseWid = Number(req.params.wid);
        const expenses:Expense[] = await expenseService.retrieveExpensesByWid(expenseWid);
        res.send(expenses);
    }catch (error) {
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

// Post expense
app.post("/expenses", async (req, res) =>{
    let expense:Expense = req.body;
    expense = await expenseService.registerExpense(expense);
    res.send(expense);
    res.status(201);
});

// Put expense
app.put("/expenses/:id", async (req, res) =>{
    try{
        const newExpense:Expense = req.body;
        newExpense.eId = Number(req.params.id);
        const expense:Expense = await expenseService.modifyExpense(newExpense);
        res.send(expense);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
})

// Delete expense
app.delete("/expenses/:id", async (req, res) =>{
    //const expenseId = Number(req.params.id);
    try{
        const expenseId = Number(req.params.id);
        const success:Boolean = await expenseService.removeExpenseById(expenseId);
        res.send(success);
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404);
            res.send(error);
        }
    }
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, ()=>{console.log("Application Started")})
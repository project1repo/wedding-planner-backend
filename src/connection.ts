import {Client} from 'pg';
require('dotenv').config({path:'D:\\Revature\\Project1App\\Project1\\app.env'})

export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSWORD,
    database:process.env.DATABASENAME,
    port:5432,
    host:'35.243.184.21'
})
client.connect();
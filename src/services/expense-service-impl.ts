import { ExpenseDAO } from "../daos/expense-dao";
import { ExpenseDaoPostgres } from "../daos/expense-dao-postgres";
import { Expense } from "../entities";
import ExpenseService from "./expense-service";





export class ExpenseServiceImpl implements ExpenseService{

    expenseDAO:ExpenseDAO = new ExpenseDaoPostgres();

    registerExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.createExpense(expense);
    }

    retrieveAllExpenses(): Promise<Expense[]> {
        return this.expenseDAO.getAllExpenses();
    }
    
    retrieveExpensesByWid(wId: number): Promise<Expense[]> {
        return this.expenseDAO.getExpensesByWid(wId);
    }

    retrieveExpenseById(expenseId: number): Promise<Expense> {
        return this.expenseDAO.getExpenseById(expenseId);
    }

    modifyExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.updateExpense(expense);
    }
    
    removeExpenseById(expenseId: number): Promise<boolean> {
        return this.expenseDAO.deleteExpenseById(expenseId);
    }

}
import { client } from "../connection";
import { Expense } from "../entities";
import { MissingResourceError } from "../errors";
import { ExpenseDAO } from "./expense-dao";




export class ExpenseDaoPostgres implements ExpenseDAO{

    async createExpense(expense: Expense): Promise<Expense> {
        const sql:string = "insert into expense(reason,amount,wedding_id) values ($1,$2,$3) returning e_id";
        const values = [expense.reason, expense.amount, expense.weddingId];
        const result = await client.query(sql, values);
        expense.eId = result.rows[0].e_id;
        return expense;
    }
    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = 'select * from expense';
        const result = await client.query(sql);
        const expenses:Expense[] = [];
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.e_id,
                row.reason,
                row.amount,
                row.wedding_id);
            expenses.push(expense);
        }
        return expenses;
    }
    async getExpenseById(expenseId: number): Promise<Expense> {
        const sql:string = 'select * from expense where e_id = $1';
        const values = [expenseId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expenseId} does not exist`);
        }
        const row = result.rows[0];
        const expense:Expense = new Expense(
                row.e_id,
                row.reason,
                row.amount,
                row.wedding_id);
        return expense;
    }
    async getExpensesByWid(expenseWid: number): Promise<Expense[]> {
        const sql:string = 'select * from expense where wedding_id = $1';
        const values = [expenseWid];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`There are no expenses with wId of ${expenseWid}`)
        }
        const expenses:Expense[] = [];
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.e_id,
                row.reason,
                row.amount,
                row.Wedding_id);
                expenses.push(expense);
        }
        return expenses;
    }
    async updateExpense(expense: Expense): Promise<Expense> {
        const sql:string = 'update expense set reason = $1, amount = $2, wedding_id = $3 where e_id = $4'
        const values = [expense.reason, expense.amount, expense.weddingId, expense.eId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expense.eId} does not exist`);
        }
        return expense;
    }
    async deleteExpenseById(expenseId:number): Promise<boolean> {
        const sql:string = 'delete from expense where e_id = $1';
        const values = [expenseId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expenseId} does not exist`);
        }
        return true;
    }

}
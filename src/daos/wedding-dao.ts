import { Wedding } from "../entities";


export interface WeddingDAO{


    createWedding(wedding:Wedding):Promise<Wedding>;

    getAllWeddings():Promise<Wedding[]>;

    getWeddingById(weddingId:number):Promise<Wedding>;

    updateWedding(wedding:Wedding, weddingId:number):Promise<Wedding>;

    deleteWeddingById(weddingId:number):Promise<boolean>;
}
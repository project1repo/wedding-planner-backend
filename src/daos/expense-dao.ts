import { Expense } from "../entities";


export interface ExpenseDAO{


    createExpense(expense:Expense):Promise<Expense>;

    getAllExpenses():Promise<Expense[]>;

    getExpenseById(expenseId:number):Promise<Expense>;

    getExpensesByWid(expenseWid:number):Promise<Expense[]>;

    updateExpense(expense:Expense):Promise<Expense>;

    deleteExpenseById(expenseId:number):Promise<boolean>;
}